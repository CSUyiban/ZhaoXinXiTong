<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 数据导入和到处
 * @package CodeIgniter
 * @subpackage Libraries
 * @category  PHPExcel
 * @author hhb <huhaobin110@gmail.com>
 * @date 20170103
 */

require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';
require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel/Reader/Excel5.php';
require_once "ChromePhp.php";
class DataOption
{

  // function __construct()
  // {
  //   # code...
  // }

  public function Export($title='中南易班',
                          $subtitle=array('A1'=>'姓名','B1'=>'易班id'),
                          $data=array('0'=>'胡皓斌','1'=>7041045),
                          $filename='中南易班.xls',
                          $photono=5990//要出入图片的列,第一列为1,不能导出5990列
                          )
  {
    // $data = $this->Question_bank_model->admin(2)['list'];
    $objPHPExcel = new PHPExcel();
    /*实例化excel图片处理类*/
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $iofactory = new PHPExcel_IOFactory();
    //Excel表格式,
    // var_dump($data);echo "<br \>";
    // exit;

    $key = 0;
    for($i=0;$i<count($subtitle);$i++)
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue(array_keys($subtitle)[$i], array_values($subtitle)[$i]);
    foreach ($data as $row => $value) {
        $shabi='A';$i=0;
        $key += 2;
        /*设置表格宽度*/
        if($photono != 5990)
        {
          $photoColumn = 'A';
          for ($x=1; $x < $photono; $x++) {
            $photoColumn ++;
          }
          $objPHPExcel->getActiveSheet()->getColumnDimension($photoColumn)->setWidth(20);
        }

        ChromePhp::log($shabi);
        /*设置表格高度*/
        $objPHPExcel->getActiveSheet()->getRowDimension($key)->setRowHeight(100);
        foreach ($value as $value) {
          // echo $key."shabi".$shabi.'value';print_r(($value));echo "<br \>";
          $i ++;

          if($photono != 5990)
          {
            if ($i == $photono) {
            /*实例化插入图片类*/
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            /*设置图片路径 切记：只能是本地图片*/
            // $objDrawing->setPath($value);
            $goal = substr_replace($value,FCPATH,0,20);
            ChromePhp::warn($goal);
            $objDrawing->setPath($goal);

            /*设置图片高度*/
            $objDrawing->setHeight(100);
            /*设置图片要插入的单元格*/
            $objDrawing->setCoordinates(($shabi++).$key);
            /*设置图片所在单元格的格式*/
            $objDrawing->setOffsetX(0);
            $objDrawing->setRotation(0);
            $objDrawing->getShadow()->setVisible(true);
            $objDrawing->getShadow()->setDirection(0);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            continue;
            }
          }
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($shabi++).$key, ($value));
        }
    }
  // }

  // exit;
  //excel保存在根目录下  如要导出文件，以下改为注释代码
  // $objPHPExcel->getActiveSheet() -> setTitle('中南易班考试系统');
  // $objPHPExcel-> setActiveSheetIndex(0);
  // $objWriter = $iofactory -> createWriter($objPHPExcel, 'Excel2007');
  // $objWriter -> save('YibanEXAM'.date('Y-m-d h:i:s').'.xlsx');
  //导出代码
  $objPHPExcel->getActiveSheet()->setTitle($title);
  $objPHPExcel->setActiveSheetIndex(0);
  $objWriter = $iofactory->createWriter($objPHPExcel, 'Excel2007');
  $filename = $filename;
  //清除php缓存防止乱码
  ob_end_clean();
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename="'.$filename.'"');
  header('Cache-Control: max-age=0');
  $objWriter->save('php://output');
  }
}
