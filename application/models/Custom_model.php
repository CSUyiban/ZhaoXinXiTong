<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 管理系统
 */
class Custom_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->dbforge();
  }

  /**
   * 查看该社团的所有报名信息
   */
  public function AllList($Community)
  {
    $query = $this->db->get_where("record",array('goal_shetuan'=>$Community));
    return $query->result_array();
  }

  /**
   * 获取某个表的表头
   */
  public function ExportDataList($value='')
  {
    return $this->db->list_fields($value);
  }
  public function HomeData($value='')
  {
    $this->db->select('name');
    $query = $this->db->get_where('Community',array('name1'=>$value));
    $result['name'] = $query->row_array();

    $this->db->select('name,regtime');
    $this->db->order_by('regtime','DESC');
    $query = $this->db->get_where('record', array('goal_shetuan'=>$value), 10, 0);
    $result['lists'] = $query->result_array();

    return $result;
  }

  public function SuperYBCheck($yb_userid='')
  {
    $query = $this->db->get_where('adminpri',array('yb_userid'=>$yb_userid,
                                          'belongto' => 'znyb',
                                          'grade'    => 0));
    if(count($query->result_array())>0)
    {
      return TRUE;
    }
    else {
      return false;
    }
  }

  /**
   * 设置组织结构
   */
  public function Set($shetuan='',$bumen='',$zu='')
  {
    $query = $this->db->get($shetuan);
    if($query->num_rows()!=0)
    {
      return false;
    }
    foreach ($bumen as $key => $value) {
      $this->db->insert($shetuan, array('title'=>$value,'grade'=>'0'));
    }

    $bumen_ = 0;       //部门循环id
    foreach ($zu as $key => $value) {
      foreach ($value as $detail) {
        $this->db->insert($shetuan, array('title'=>$detail,'grade'=>$bumen[$key]));
      }
      $bumen_ ++;
    }


  }
  public function SuperCheck($yb_userid='')
  {
    $query = $this->db->get_where('adminpri',array('yb_userid'=>$yb_userid,
                                          'grade'    => 0));
    if(count($query->result_array())>0)
    {
      return TRUE;
    }
    else {
      return false;
    }
  }

  public function AdminCheck($yb_userid='')                                     //管理员身份检查
  {
    $query = $this->db->get_where('adminpri',array('yb_userid'=>$yb_userid));
    $result = $query->row_array();

    if(count($result)<=0)
      return false;
    else {
      return $result;
    }
  }

  public function AdminList($Community='')
  {
    $this->db->select("name,email,grade,yb_userid,telephone");
    $query = $this->db->get_where('adminpri',array('belongto'=>$Community));
    return $query->result_array();
  }

  public function AddAdmin($data)
  {
    $this->db->insert('adminpri',$data);
  }

  public function DeleteAdmin($yb_userid)
  {
    $this->db->delete('adminpri', array('yb_userid' => $yb_userid));
  }

  public function CommunityList($value='')
  {
    $this->db->select("name,name1,gradenum,description,logo");
    $query = $this->db->get('Community');
    return $query->result_array();
  }

  public function AddCommunity($data)
  {
    $this->db->insert('Community',$data);
    $this->CreateTable($data['name1']);
  }

  public function DeleteCommunity($name1)
  {
    $this->db->delete('Community', array('name1' => $name1));
    return $this->dbforge->drop_table($name1,TRUE);
  }

  /**
   * 新建各个社团自己的表
   */
  public function CreateTable($name1)
  {
    $this->dbforge->add_field('id');
    $attributes = array('ENGINE' => 'InnoDB');
    $fields = array(
                    'title' => array(
                                'type' => 'TEXT',
                                'comment' => '部门或组的名字'
                              ),
                    'grade' => array(
                                'type' => 'TEXT',
                                'comment' => '标志当前是部门还是组'
                              )
                    );
    $this->dbforge->add_field($fields);

    return $this->dbforge->create_table($name1, true, $attributes);
  }
}
