<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Recuit_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

/**
 * @ input  社团的拼音缩写
 */
  public function homedata($value='znyb')
  {
    $query['bumen']    = $this->db->get_where($value,array('grade' => '0'));
    $sql = "select title,grade from $value where grade != '0'";
    $query['structure'] = $this->db->get_where('Community',array('name1' => $value));
    $query['bumen']    = $query['bumen']   ->result_array();

    foreach ($query['bumen'] as $key => $temp) {
      $query['zu'][$key] = $this->db->get_where($value,array('grade'=>$temp['title']));
      $query['zu'][$key] = $query['zu'][$key]->result_array();
    }
    $query['structure'] = $query['structure']->row_array();
    return $query;
  }

  public function reg($data="")
  {
    $this->db->insert('record',$data);
  }
}
