<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 招新报名
 */
class Recuit extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $params = array('app' => 'recuit');
    $this->load->library('Use_smarty', $params, 'smarty');
    $this->load->model('Recuit_model');
    $this->load->helper(array('form', 'url'));//上传
  }

  public function index()
  {
    $this->doupload('znyb');
  }

  public function intro($value='xsh')
  {
    $data['data'] = $this->Recuit_model->homedata($value);
    $data['intro'] = array_reverse(explode("##",$data['data']["structure"]["description"]));
    $data['shetuan'] = $value;
    $this->smarty->tplview('intro',$data);
  }

  /**
   * 扫码进入的系统页面(新生报名页面)
   * @input  社团的拼音缩写
   */
  public function doupload($shetuan='znyb')
  {
       $config['upload_path']      = './uploads/Recuit';
       $config['allowed_types']    = 'gif|jpg|png';
       $config['max_size']         = 1024;
       $config['max_width']        = 1024;
       $config['max_height']       = 768;
       $config['encrypt_name']     = true;

       $this->load->library('upload', $config);

       if ( ! $this->upload->do_upload('photo') )
       {
         $data['description'] = $this->Recuit_model->homedata($shetuan);

        //  print_r($data['description']);
         $data['error']   = $this->upload->display_errors();
         $data['shetuan'] = $shetuan;
         $this->smarty->tplview('index',$data);
       }
       else
       {
           $data['upload_data'] = $this->upload->data();
           $uploaded = $this->input->post();
           $uploaded['goal_shetuan'] = $shetuan;
           $uploaded['pathofphoto'] = base_url().'uploads/Recuit/'.$data['upload_data']['file_name'];       //待保存的文件名
           $this->Recuit_model->reg($uploaded);
           echo "<script>alert('您的报名信息已提交,请耐心等待通知');history.go(-1);</script>";
       }

     }
}
