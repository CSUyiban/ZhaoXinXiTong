<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">操作管理员</h1>
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情
              </div>
              <!-- .panel-heading -->
              <div class="panel-body">
                  <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">管理员列表</a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>姓名</th>
                                                <th>易班id</th>
                                                <th>email</th>
                                                <th>电话</th>
                                                <th>权限</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          {foreach $lists as $list}
                                            <tr class="odd gradeX">
                                                <td>{$list.name}</td>
                                                <td>{$list.yb_userid}</td>
                                                <td>{$list.email}</td>
                                                <td>{$list.telephone}</td>
                                                <td>{$list.grade}</td>
                                            </tr>
                                          {/foreach}
                                        </tbody>
                                    </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">添加管理员</a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                <form action="{$base_url}/Custom/AddAdmin" method="post">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                            <th>姓名</th>
                                            <th>易班id</th>
                                            <th>email</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr class="odd gradeX">
                                              <td><input type="text" name="name"></input></td>
                                              <td><input type="text" name="yb_userid"></input></td>
                                              <td><input type="text" name="email"></input></td>
                                          </tr>
                                          <tr class="even gradeX">
                                              <td><input type="text" name="telephone" placeholder="电话"></input></td>
                                              <td><input type="text" name="grade" placeholder="权限"></input></td>
                                              {if isset($EditCommunity)}
                                              <td>
                                                <label>所属组织</label>
                                                <select class="form-control" name="belongto">
                                                {foreach $C_lists as $list}
                                                  <option value="{$list.name1}">{$list.name}</option>
                                                {/foreach}
                                              </select></td>
                                              {/if}
                                          </tr>
                                      </tbody>
                                  </table>
                                  <button class="btn btn-outline btn-primary btn-lg btn-block">添加</button>
                                </form>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">删除管理员</a>
                              </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                              <th>姓名</th>
                                              <th>易班id</th>
                                              <th>email</th>
                                              <th>电话</th>
                                              <th>权限</th>
                                              <th>操作</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        {foreach $lists as $list}
                                          <tr class="odd gradeX">
                                              <td>{$list.name}</td>
                                              <td>{$list.yb_userid}</td>
                                              <td>{$list.email}</td>
                                              <td>{$list.telephone}</td>
                                              <td>{$list.grade}</td>
                                              <td><a href="{$base_url}Custom/DeleteA/{$list.yb_userid}">删除</a></td>
                                          </tr>
                                        {/foreach}
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- .panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
