<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">全部报名信息</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                      <thead>
                          <tr>
                              <th>姓名</th>
                              <th>电话</th>
                              <th>QQ</th>
                              <th>寝室</th>
                              <th>意愿</th>
                              <th>照片</th>
                          </tr>
                      </thead>
                      <tbody>
                        {if empty($lists)}
                          暂时没有报名信息,请稍后查看
                        {else}
                          {foreach $lists as $list}
                            <tr class="odd gradeX">
                                <td>{$list.name}</td>
                                <td>{$list.telephone}</td>
                                <td>{$list.qq}</td>
                                <td>{$list.dormitory}</td>
                                <td>{$list.goal_bumen} | {$list.goal_zu}</td>
                                <td><a href={$list.pathofphoto} target="_blank">点击查看</a></td>
                            </tr>
                          {/foreach}
                        {/if}
                      </tbody>
                  </table>
              </div>
              <!-- /.panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
