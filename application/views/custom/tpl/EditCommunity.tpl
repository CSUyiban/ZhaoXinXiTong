<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">社团管理</h1>
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情请参照<a href="{$base_url}Recuit/doupload/znyb" target="_blank">中南易班</a>报名页格式
              </div>
              <!-- .panel-heading -->
              <div class="panel-body">
                  <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">社团列表</a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>社团名</th>
                                                <th>社团拼音首字母</th>
                                                <th>社团下设级别数</th>
                                                {*<th>社团介绍</th>*}
                                                <th>logo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          {foreach $lists as $list}
                                            <tr class="odd gradeX">
                                              <td>{$list.name}</td>
                                              <td>{$list.name1}</td>
                                              <td>{$list.gradenum}</td>
                                              {*<td>{$list.description}</td>*}
                                              <td><a href="{$list.logo}" target="_blank">点击查看</td>
                                            </tr>
                                          {/foreach}
                                        </tbody>
                                    </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">添加社团</a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                {$error}
                                <form action="{$base_url}Custom/EditCommunity"  enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <!-- <form action="{$base_url}Custom/AddCommunity" method="post"> -->
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <!-- <thead>
                                          <tr>
                                            <th>社团名</th>
                                            <th>社团拼音首字母</th>
                                            <th>社团下设级别数</th>
                                            <th>社团介绍(请上传pdf文档)</th>
                                            <th>logo(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                          </tr>
                                      </thead> -->
                                      <tbody>
                                          <tr class="odd gradeX">
                                              <td><input type="text" name="name" placeholder="社团名"></input></td>
                                              <td><input type="text" name="name1" placeholder="社团拼音首字母"></input></td>
                                              <td><input type="text" name="gradenum" placeholder="社团下设级别数"></input></td>
                                          </tr>
                                          <tr class="even gradeX">
                                            <td>社团介绍(请上传pdf文档)<input type="file" name="pdf"></input></td>
                                            <td>logo(大小:)<input type="file" name="logo"></input></td>
                                            <td>轮播图(大小:)<input type="file" name="phpto1"></input></td>

                                          </tr>
                                          <tr class="even gradeX">
                                            <td>轮播图(大小:)<input type="file" name="phpto2"></input></td>
                                            <td>轮播图(大小:)<input type="file" name="phpto3"></input></td>
                                          </tr>
                                      </tbody>
                                  </table>
                                  <button class="btn btn-outline btn-primary btn-lg btn-block">添加</button>
                                </form>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">删除社团</a>
                              </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                            <th>社团名</th>
                                            <th>社团拼音首字母</th>
                                            <th>社团下设级别数</th>
                                            {*<th>社团介绍</th>*}
                                            <th>logo</th>
                                            <th>操作</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        {foreach $lists as $list}
                                          <tr class="odd gradeX">
                                              <td>{$list.name}</td>
                                              <td>{$list.name1}</td>
                                              <td>{$list.gradenum}</td>
                                              {*<td>{$list.description}</td>*}
                                              <td><a href="{$list.logo}" target="_blank">点击查看</td>
                                              <td><a href="{$base_url}/Custom/DelateC/{$list.name1}">删除</a></td>
                                          </tr>
                                        {/foreach}
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">设置社团结构(下设数为0的无法设置)</a>
                              </h4>
                          </div>
                          <div id="collapsefour" class="panel-collapse collapse">
                              <div class="panel-body">
                                <form action="{$base_url}Custom/Set" method="post">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <tbody>
                                      <tr class="odd gradeX">
                                        <label>请选择要操作的社团</label>
                                        <select class="form-control" name="shetuan">
                                          {foreach $lists as $list}
                                            <option value="{$list.name1}">{$list.name}</option>
                                          {/foreach}
                                        </select>
                                      </tr>
                                      <tr class="odd gradeX">
                                        <td><input type="text" name ="bumen" class="form-control" placeholder="请输入该社团的部门(格式:运营部##技术部##行政部##宣传部##美工部)"></input></td>
                                      </tr>
                                      <tr class="odd gradeX">
                                        <td><input type="text" name ="zu" class="form-control" placeholder="请输入该社团的组(同一部门组之间用'**'隔开,不同部门组之间用'##'隔开"></input></td>
                                      </tr>
                                    </tbody>

                                    </table>
                                    <button class="btn btn-outline btn-primary btn-lg btn-block">进行设置</button>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- .panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
