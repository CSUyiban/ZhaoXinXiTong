</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="{$base_url}js/recuit/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{$base_url}js/recuit/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{$base_url}js/recuit/metisMenu.min.js"></script>

<script src="{$base_url}js/recuit/jquery.dataTables.min.js"></script>
<script src="{$base_url}js/recuit/dataTables.bootstrap.min.js"></script>
<script src="{$base_url}js/recuit/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="{$base_url}js/recuit/sb-admin-2.js"></script>
<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>
</body>

</html>
