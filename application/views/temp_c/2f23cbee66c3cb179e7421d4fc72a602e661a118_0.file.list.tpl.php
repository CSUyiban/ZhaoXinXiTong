<?php
/* Smarty version 3.1.30, created on 2017-08-07 12:58:01
  from "/var/www/html/ci/application/views/custom/tpl/list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5987f359545883_05014895',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f23cbee66c3cb179e7421d4fc72a602e661a118' => 
    array (
      0 => '/var/www/html/ci/application/views/custom/tpl/list.tpl',
      1 => 1502081873,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5987f359545883_05014895 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">全部报名信息</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                      <thead>
                          <tr>
                              <th>姓名</th>
                              <th>电话</th>
                              <th>QQ</th>
                              <th>寝室</th>
                              <th>意愿</th>
                              <th>照片</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php if (empty($_smarty_tpl->tpl_vars['lists']->value)) {?>
                          暂时没有报名信息,请稍后查看
                        <?php } else { ?>
                          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                            <tr class="odd gradeX">
                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['telephone'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['qq'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['dormitory'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['goal_bumen'];?>
 | <?php echo $_smarty_tpl->tpl_vars['list']->value['goal_zu'];?>
</td>
                                <td><a href=<?php echo $_smarty_tpl->tpl_vars['list']->value['pathofphoto'];?>
 target="_blank">点击查看</a></td>
                            </tr>
                          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        <?php }?>
                      </tbody>
                  </table>
              </div>
              <!-- /.panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
<?php }
}
