<?php
/* Smarty version 3.1.30, created on 2017-07-28 16:20:32
  from "/var/www/html/ci/application/views/custom/tpl/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_597af3d097b2b1_64376901',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd074cf3bbda77ece7852dab029c6f6b46e8ec5f5' => 
    array (
      0 => '/var/www/html/ci/application/views/custom/tpl/index.tpl',
      1 => 1500474461,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_597af3d097b2b1_64376901 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $_smarty_tpl->tpl_vars['home']->value['name']['name'];?>
</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    
    <!-- /.row -->
    <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <i class="fa fa-bell fa-fw"></i> 最新报名情况
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <div class="list-group">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['home']->value['lists'], 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                      <a href="#" class="list-group-item">
                          <i class="fa fa-comment fa-fw"></i> <?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>

                          <span class="pull-right text-muted small"><em><?php echo $_smarty_tpl->tpl_vars['list']->value['regtime'];?>
</em>
                          </span>
                      </a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                  </div>
              </div>
              <!-- /.panel-body -->
          </div>
      </div>
    </div>
<?php }
}
