<?php
/* Smarty version 3.1.30, created on 2017-07-28 16:20:42
  from "/var/www/html/ci/application/views/custom/tpl/EditCommunity.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_597af3da1a6ab9_10241780',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dc2253d5ac398332f87c61595d046466442c12b7' => 
    array (
      0 => '/var/www/html/ci/application/views/custom/tpl/EditCommunity.tpl',
      1 => 1501212579,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_597af3da1a6ab9_10241780 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">社团管理</h1>
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情请参照<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Recuit/doupload/znyb" target="_blank">中南易班</a>报名页格式
              </div>
              <!-- .panel-heading -->
              <div class="panel-body">
                  <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">社团列表</a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>社团名</th>
                                                <th>社团拼音首字母</th>
                                                <th>社团下设级别数</th>
                                                
                                                <th>logo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                            <tr class="odd gradeX">
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name1'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['gradenum'];?>
</td>
                                              
                                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['list']->value['logo'];?>
" target="_blank">点击查看</td>
                                            </tr>
                                          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                        </tbody>
                                    </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">添加社团</a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                <?php echo $_smarty_tpl->tpl_vars['error']->value;?>

                                <form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Custom/EditCommunity"  enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                <!-- <form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Custom/AddCommunity" method="post"> -->
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <!-- <thead>
                                          <tr>
                                            <th>社团名</th>
                                            <th>社团拼音首字母</th>
                                            <th>社团下设级别数</th>
                                            <th>社团介绍(请上传pdf文档)</th>
                                            <th>logo(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                            <th>轮播图(大小:)</th>
                                          </tr>
                                      </thead> -->
                                      <tbody>
                                          <tr class="odd gradeX">
                                              <td><input type="text" name="name" placeholder="社团名"></input></td>
                                              <td><input type="text" name="name1" placeholder="社团拼音首字母"></input></td>
                                              <td><input type="text" name="gradenum" placeholder="社团下设级别数"></input></td>
                                          </tr>
                                          <tr class="even gradeX">
                                            <td>社团介绍(请上传pdf文档)<input type="file" name="pdf"></input></td>
                                            <td>logo(大小:)<input type="file" name="logo"></input></td>
                                            <td>轮播图(大小:)<input type="file" name="phpto1"></input></td>

                                          </tr>
                                          <tr class="even gradeX">
                                            <td>轮播图(大小:)<input type="file" name="phpto2"></input></td>
                                            <td>轮播图(大小:)<input type="file" name="phpto3"></input></td>
                                          </tr>
                                      </tbody>
                                  </table>
                                  <button class="btn btn-outline btn-primary btn-lg btn-block">添加</button>
                                </form>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">删除社团</a>
                              </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                            <th>社团名</th>
                                            <th>社团拼音首字母</th>
                                            <th>社团下设级别数</th>
                                            
                                            <th>logo</th>
                                            <th>操作</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                          <tr class="odd gradeX">
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name1'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['gradenum'];?>
</td>
                                              
                                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['list']->value['logo'];?>
" target="_blank">点击查看</td>
                                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/Custom/DelateC/<?php echo $_smarty_tpl->tpl_vars['list']->value['name1'];?>
">删除</a></td>
                                          </tr>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">设置社团结构(下设数为0的无法设置)</a>
                              </h4>
                          </div>
                          <div id="collapsefour" class="panel-collapse collapse">
                              <div class="panel-body">
                                <form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Custom/Set" method="post">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <tbody>
                                      <tr class="odd gradeX">
                                        <label>请选择要操作的社团</label>
                                        <select class="form-control" name="shetuan">
                                          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['list']->value['name1'];?>
"><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</option>
                                          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                        </select>
                                      </tr>
                                      <tr class="odd gradeX">
                                        <td><input type="text" name ="bumen" class="form-control" placeholder="请输入该社团的部门(格式:运营部##技术部##行政部##宣传部##美工部)"></input></td>
                                      </tr>
                                      <tr class="odd gradeX">
                                        <td><input type="text" name ="zu" class="form-control" placeholder="请输入该社团的组(同一部门组之间用'**'隔开,不同部门组之间用'##'隔开"></input></td>
                                      </tr>
                                    </tbody>

                                    </table>
                                    <button class="btn btn-outline btn-primary btn-lg btn-block">进行设置</button>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- .panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
<?php }
}
