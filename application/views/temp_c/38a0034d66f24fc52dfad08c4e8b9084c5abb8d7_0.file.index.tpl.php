<?php
/* Smarty version 3.1.30, created on 2017-07-31 05:23:30
  from "/var/www/html/ci/application/views/recuit/tpl/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_597e4e52236930_34550030',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '38a0034d66f24fc52dfad08c4e8b9084c5abb8d7' => 
    array (
      0 => '/var/www/html/ci/application/views/recuit/tpl/index.tpl',
      1 => 1501225012,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_597e4e52236930_34550030 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- 标签<b>中的组织/社团名 & <img>中的社团/组织标志 随扫码变化而变化-->
  <div id="organ">
    <span>当前报名社团/组织：</span><b style="color: #A52A2A"><?php echo $_smarty_tpl->tpl_vars['description']->value['structure']['name'];?>
</b>
    <img src="<?php echo $_smarty_tpl->tpl_vars['description']->value['structure']['logo'];?>
">
  </div>
<!-- **********************************************************************-->


<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
recuit/doupload" name="myform" enctype="multipart/form-data" method="post" accept-charset="utf-8" onsubmit="return checki()">
    <fieldset>
      
      <label id="lb1">姓名：</label><input type="text" name="name"><br><hr>
      <label id="lb1">学号：</label><input type="text" name="StudentID"><br><hr>
      <label id="lb1">性别：</label><div id="sxp1" class="img-circle"><input type="radio" name="sex" id="sx" value="女" onclick="radio1()"></div><div id="sxp2" class="img-circle"><input type="radio" name="sex" id="sx" value="男" onclick="radio2()"></div><br><hr>
      <label id="lb1">政治面貌：</label><input type="text" name="political_status"><br><hr>
      <label id="lb1">出生日期：</label><input type="text" name="date_of_birth"><br><hr>
      <div id="photo">
        <span style="margin:20px;line-height: 70px;">照片:<br></span>
        <div id="localImag"><img id="preview" src=""><span id="sp3">+</span><input type="file" name="photo" id="doc"  onchange="javascript:setImagePreview();"></div>
      </div><hr>
      <label id="lb1">学院：</label>
        <select id="sel1" name="college">
          <option value="资源与安全工程学院" selected="selected">资源与安全工程学院</option>
              <option value="资源加工与生物工程学院">资源加工与生物工程学院</option>
              <option value="冶金与环境学院">冶金与环境学院</option>
              <option value="信息科学与工程学院">信息科学与工程学院</option>
              <option value="物理与电子学院">物理与电子学院</option>
              <option value="能源科学与工程学院">能源科学与工程学院</option>
              <option value="材料科学与工程学院">材料科学与工程学院</option>
              <option value="粉末冶金研究院">粉末冶金研究院</option>
              <option value="航空航天学院">航空航天学院</option>
              <option value="交通运输工程学院">交通运输工程学院</option>
              <option value="机电工程学院">机电工程学院</option>
              <option value="化学化工学院">化学化工学院</option>
              <option value="土木工程学院">土木工程学院</option>
              <option value="软件学院">软件学院</option>
              <option value="数学与统计学院">数学与统计学院</option>
              <option value="地球科学与信息物理学院">地球科学与信息物理学院</option>
              <option value="文学院">文学院</option>
              <option value="外国语学院">外国语学院</option>
              <option value="商学院">商学院</option>
              <option value="马克思主义学院">马克思主义学院</option>
              <option value="建筑与艺术学院">建筑与艺术学院</option>
              <option value="体育教研部">体育教研部</option>
              <option value="公共管理学院">公共管理学院</option>
              <option value="法学院">法学院</option>
              <option value="湘雅医学院">湘雅医学院</option>
              <option value="基础医学院">基础医学院</option>
              <option value="口腔医学院">口腔医学院</option>
              <option value="公共卫生学院">公共卫生学院</option>
              <option value="生物科学学院">生物科学学院</option>
              <option value="药学院">药学院</option>
              <option value="护理学院">护理学院</option>
        </select><br><hr>
      <label id="lb1">专业班级:</label><input type="text" name="professional_class"><br><hr>
      <label id="lb1">寝室：</label><input type="text" name="dormitory"><br><hr>
      <label id="lb1">籍贯：</label><input type="text" name="native_place"><br><hr>
      <label id="lb1">手机：</label><input type="text" name="telephone"><br><hr>
      <label id="lb1">QQ：</label><input type="text" name="qq"><br><hr>
      <label id="lb1">特长：</label><input type="text" name="specialty"><br><hr>
<!-- 二级联动下拉列表-->
      <label id="lb1">竞聘部门：</label><br><span style="color: #006699;margin-left: 40px;">志愿：</span>
      <label id="lb2">部门：</label>
      <select  name="goal_bumen" id="bigname" onChange="getCity()">
          <option value="no">请选择</option>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['description']->value['bumen'], 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</option>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

      </select>
      <label id="lb2">组别：</label>
      <select  name="goal_zu" id="smallname">
        <option value="no">请选择</option>
      </select>
      <label id="lb1">是否服从调剂：</label>
      <div class="slideOne">
        <span id="s1">是</span><input type="checkbox" value="None" id="slideOne" name="adjust" checked />
        <label for="slideOne"></label><span id="s1">否</span>
    </div><hr>
      <label id="lb1">个人基本情况：</label><br>
      <textarea name="personal_evaluation" id="t">（请从学习成绩、任职经历、获奖情况、个人特长、爱好、申请职位相关技能等方面进行自我说明，并进行自我评价）</textarea><br><hr>
<!-- 提交按钮-->
      <button class="btn" id="btn" >提交</button>
      
    </fieldset>
  </form>
<!-- 将表单数据提交到iframe中，防止提交按钮自动刷新界面-->
  <!-- <iframe id="rfFrame" name="rfFrame" src="about:blank" style="display:none;"></iframe> -->
<?php }
}
