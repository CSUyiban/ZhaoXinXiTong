<?php
/* Smarty version 3.1.30, created on 2017-08-06 19:55:42
  from "/var/www/html/ci/application/views/custom/tpl/opt.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_598703be4b3f03_64826156',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'acd4c018771770ecbdff329ceb21e67bd3367e3e' => 
    array (
      0 => '/var/www/html/ci/application/views/custom/tpl/opt.tpl',
      1 => 1502020432,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598703be4b3f03_64826156 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">操作管理员</h1>
          <div class="panel panel-default">
              <div class="panel-heading">
                  详情
              </div>
              <!-- .panel-heading -->
              <div class="panel-body">
                  <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">管理员列表</a>
                              </h4>
                          </div>
                          <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>姓名</th>
                                                <th>易班id</th>
                                                <th>email</th>
                                                <th>电话</th>
                                                <th>权限</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['yb_userid'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['email'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['telephone'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['list']->value['grade'];?>
</td>
                                            </tr>
                                          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                        </tbody>
                                    </table>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">添加管理员</a>
                              </h4>
                          </div>
                          <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                <form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/Custom/AddAdmin" method="post">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                            <th>姓名</th>
                                            <th>易班id</th>
                                            <th>email</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr class="odd gradeX">
                                              <td><input type="text" name="name"></input></td>
                                              <td><input type="text" name="yb_userid"></input></td>
                                              <td><input type="text" name="email"></input></td>
                                          </tr>
                                          <tr class="even gradeX">
                                              <td><input type="text" name="telephone" placeholder="电话"></input></td>
                                              <td><input type="text" name="grade" placeholder="权限"></input></td>
                                              <?php if (isset($_smarty_tpl->tpl_vars['EditCommunity']->value)) {?>
                                              <td>
                                                <label>所属组织</label>
                                                <select class="form-control" name="belongto">
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['C_lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                                  <option value="<?php echo $_smarty_tpl->tpl_vars['list']->value['name1'];?>
"><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</option>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                              </select></td>
                                              <?php }?>
                                          </tr>
                                      </tbody>
                                  </table>
                                  <button class="btn btn-outline btn-primary btn-lg btn-block">添加</button>
                                </form>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">删除管理员</a>
                              </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                      <thead>
                                          <tr>
                                              <th>姓名</th>
                                              <th>易班id</th>
                                              <th>email</th>
                                              <th>电话</th>
                                              <th>权限</th>
                                              <th>操作</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lists']->value, 'list');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['list']->value) {
?>
                                          <tr class="odd gradeX">
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['name'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['yb_userid'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['email'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['telephone'];?>
</td>
                                              <td><?php echo $_smarty_tpl->tpl_vars['list']->value['grade'];?>
</td>
                                              <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Custom/DeleteA/<?php echo $_smarty_tpl->tpl_vars['list']->value['yb_userid'];?>
">删除</a></td>
                                          </tr>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- .panel-body -->
          </div>
          <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
  </div>
</div>
<?php }
}
