<?php
/* Smarty version 3.1.30, created on 2017-07-31 05:23:30
  from "/var/www/html/ci/application/views/recuit/tpl/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_597e4e52092c50_77015029',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b494b2cf8af4148582ab7e4a2b28c0869bfafad7' => 
    array (
      0 => '/var/www/html/ci/application/views/recuit/tpl/header.tpl',
      1 => 1501224780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_597e4e52092c50_77015029 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
   <head>
      <title>填写报名表</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
      <link href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
css/recuit/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
css/recuit/sign_up.css">
   </head>
   <body>
   		<header>
   			<div class="head">
   				<h4>中南大学社团招新啦</h4>
   			</div>
   			<div class="nav">
     			<ul id="u">
     				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Recuit/doupload/<?php echo $_smarty_tpl->tpl_vars['shetuan']->value;?>
">填写报名表</a></li>
     				<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
Recuit/intro/<?php echo $_smarty_tpl->tpl_vars['shetuan']->value;?>
"> 职能简介 </a></li>
     			</ul>
     		</div>
 	<!-- 提交后验证弹窗-->
     		<div id="myAlert1" class="alert alert-success" style="display: none;">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>提交成功！</strong>请耐心等待结果通知。
    		</div>
    		<div id="myAlert2" class="alert alert-warning" style="display: none;background-color: #ffc0cb;color: red;">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>有信息未填写！</strong>请将信息补充完整。
    		</div>
   		</header>
<?php }
}
