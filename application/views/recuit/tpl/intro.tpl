<div id="myCarousel" class="carousel slide">
    <!-- 轮播（Carousel）指标 -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <!-- 轮播（Carousel）项目 -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="{$data.structure.image1}" alt="First slide">
      </div>
      <div class="item">
        <img src="{$data.structure.image2}" alt="Second slide">
      </div>
      <div class="item">
        <img src="{$data.structure.image3}" alt="Third slide">
      </div>
    </div>
    <!-- 轮播（Carousel）导航 -->
    <a class="carousel-control left" href="#myCarousel"
       data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel"
       data-slide="next">&rsaquo;</a>
</div>

<div id="intr">
<!-- 这块放后台读取出来的pdf文件（各个社团组织的简介文档） 我先用个图片代替了^_^ -->
  <!-- <img src="a-1.jpg" width="100%" style="margin-top: 15px"> -->
  {foreach $intro as $temp}
    <img src="{$temp}" width="100%" style="margin-top: 15px">
  {/foreach}
</div>
