<!DOCTYPE html>
<html>
   <head>
      <title>填写报名表</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
      <meta name="renderer" content="webkit"> <!-- 360手机优先使用chrome内核 -->
      <meta name="screen-orientation" content="portrait">  <!-- UC强制竖屏 -->
      <meta name="x5-orientation" content="portrait">  <!-- QQ强制竖屏 -->
      <meta name="browsermode" content="application">  <!-- UC浏览器应用模式 -->
      <!--  <meta name="x5-page-mode" content="app">  -->  <!-- QQ应用模式 -->
      <meta http-equiv="Cache-Control" content="no-siteapp"> <!-- 禁止百度转载页面加载流氓广告 -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <!-- 优先使用最高IE内核和chrome内核 -->
      <meta name="viewport" content="width=device-width,maximum-scale=1,initial-scale=1.0,user-scalable=no">  <!-- 禁止用户修改网页大小 -->
      <meta name="google" value="notranslate">  <!-- 禁用google翻译网页 -->
      <meta name="robots" content="index,follow">  <!-- 网页搜索引擎方式 -->
      <link href="{$base_url}css/recuit/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{$base_url}css/recuit/sign_up.css">
   </head>
   <body>
   		<header>
   			<div class="head">
   				<h4>中南大学社团招新啦</h4>
   			</div>
   			<div class="nav">
     			<ul id="u">
     				<li><a href="{$base_url}Recuit/doupload/{$shetuan}">填写报名表</a></li>
     				<li><a href="{$base_url}Recuit/intro/{$shetuan}"> 职能简介 </a></li>
     			</ul>
     		</div>
 	<!-- 提交后验证弹窗-->
     		<div id="myAlert1" class="alert alert-success" style="display: none;">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>提交成功！</strong>请耐心等待结果通知。
    		</div>
    		<div id="myAlert2" class="alert alert-warning" style="display: none;background-color: #ffc0cb;color: red;">
    			<a href="#" class="close" data-dismiss="alert">&times;</a>
    			<strong>有信息未填写！</strong>请将信息补充完整。
    		</div>
   		</header>
