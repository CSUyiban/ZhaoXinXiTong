-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2017-07-28 16:22:24
-- 服务器版本： 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recuit`
--

-- --------------------------------------------------------

--
-- 表的结构 `adminpri`
--

CREATE TABLE `adminpri` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL COMMENT '姓名',
  `belongto` text NOT NULL COMMENT '所属社团',
  `yb_userid` int(11) NOT NULL,
  `telephone` varchar(11) NOT NULL,
  `email` text NOT NULL,
  `grade` int(11) NOT NULL COMMENT '权限等级0为最高'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='各社团管理员';

--
-- 转存表中的数据 `adminpri`
--

INSERT INTO `adminpri` (`id`, `name`, `belongto`, `yb_userid`, `telephone`, `email`, `grade`) VALUES
(2, '胡皓斌', 'znyb', 7041045, '15675123342', 'huhaobin110@gmail.com', 0);

-- --------------------------------------------------------

--
-- 表的结构 `Community`
--

CREATE TABLE `Community` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL COMMENT '社团名字',
  `name1` text NOT NULL COMMENT '社团拼音名',
  `gradenum` int(11) NOT NULL COMMENT '机构下设级别数',
  `description` text NOT NULL COMMENT '社团介绍',
  `logo` text NOT NULL COMMENT 'logo地址',
  `image1` text NOT NULL COMMENT '展示图片1的地址',
  `image2` text NOT NULL,
  `image3` text NOT NULL,
  `image4` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='招新社团管理';

--
-- 转存表中的数据 `Community`
--

INSERT INTO `Community` (`id`, `name`, `name1`, `gradenum`, `description`, `logo`, `image1`, `image2`, `image3`, `image4`) VALUES
(17, '中南易班', 'znyb', 2, 'http://127.0.0.1/ci/uploads/Custom/9ea2a2dc9d1bcb9482f130fef11a9d11.jpg', 'http://127.0.0.1/ci/uploads/Custom/3a5f1fa0789985ccfccd729f9c241eaa.png', 'http://127.0.0.1/ci/uploads/Custom/725951575eb5fbe507dfa80c8e0c10f1.png', 'http://127.0.0.1/ci/uploads/Custom/71c54f9ea11eab6d46ab2f3c9151ef97.png', 'http://127.0.0.1/ci/uploads/Custom/ae7a173f8f9f8ef528ef68eaed5f3f76.png', '');

-- --------------------------------------------------------

--
-- 表的结构 `record`
--

CREATE TABLE `record` (
  `id` int(11) NOT NULL,
  `StudentID` varchar(10) COLLATE utf8_estonian_ci NOT NULL,
  `name` text COLLATE utf8_estonian_ci NOT NULL COMMENT '姓名',
  `pathofphoto` text COLLATE utf8_estonian_ci NOT NULL COMMENT '照片的上传地址',
  `sex` text COLLATE utf8_estonian_ci NOT NULL COMMENT '性别',
  `political_status` text COLLATE utf8_estonian_ci NOT NULL COMMENT '政治面貌',
  `date_of_birth` text COLLATE utf8_estonian_ci NOT NULL COMMENT '出生日期',
  `college` text COLLATE utf8_estonian_ci NOT NULL COMMENT '学院',
  `professional_class` text COLLATE utf8_estonian_ci NOT NULL COMMENT '专业班级',
  `dormitory` text COLLATE utf8_estonian_ci NOT NULL COMMENT '寝室',
  `telephone` varchar(11) COLLATE utf8_estonian_ci NOT NULL COMMENT '手机',
  `native_place` text COLLATE utf8_estonian_ci NOT NULL COMMENT '籍贯',
  `qq` text COLLATE utf8_estonian_ci NOT NULL,
  `specialty` text COLLATE utf8_estonian_ci NOT NULL COMMENT '特长',
  `goal_shetuan` text COLLATE utf8_estonian_ci NOT NULL COMMENT '报名社团',
  `goal_bumen` text COLLATE utf8_estonian_ci NOT NULL COMMENT '报名部门',
  `goal_zu` text COLLATE utf8_estonian_ci NOT NULL COMMENT '报名组',
  `adjust` text COLLATE utf8_estonian_ci NOT NULL COMMENT '是否服从调剂',
  `personal_evaluation` text COLLATE utf8_estonian_ci NOT NULL COMMENT '个人评价',
  `regtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '报名时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci COMMENT='报名记录';

--
-- 转存表中的数据 `record`
--

INSERT INTO `record` (`id`, `StudentID`, `name`, `pathofphoto`, `sex`, `political_status`, `date_of_birth`, `college`, `professional_class`, `dormitory`, `telephone`, `native_place`, `qq`, `specialty`, `goal_shetuan`, `goal_bumen`, `goal_zu`, `adjust`, `personal_evaluation`, `regtime`) VALUES
(13, '1402140424', '胡皓斌', 'http://127.0.0.1/ci/uploads/Recuit/e83ca8c69333433e8d5bcadee9384b70.png', '男', '团员', '19960808', '土木工程学院', '电科1404', '703', '15675123342', '甘肃', '1428132225', '的外地', 'znyb', '行政部', '', 'None', '（请从学习成绩、任职经历、获奖情况、个人特长、爱好、申请职位相关技能等方面进行自我说明，并进行自我评价）', '2017-07-27 10:39:16');

-- --------------------------------------------------------

--
-- 表的结构 `znyb`
--

CREATE TABLE `znyb` (
  `id` int(9) NOT NULL,
  `title` text NOT NULL COMMENT '部门或组的名字',
  `grade` text NOT NULL COMMENT '标志当前是部门还是组'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminpri`
--
ALTER TABLE `adminpri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `yb_userid` (`yb_userid`);

--
-- Indexes for table `Community`
--
ALTER TABLE `Community`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `znyb`
--
ALTER TABLE `znyb`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `adminpri`
--
ALTER TABLE `adminpri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `Community`
--
ALTER TABLE `Community`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- 使用表AUTO_INCREMENT `record`
--
ALTER TABLE `record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- 使用表AUTO_INCREMENT `znyb`
--
ALTER TABLE `znyb`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
