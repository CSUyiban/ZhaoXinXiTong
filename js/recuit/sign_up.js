//二级联动菜单（需要PHP）

function getSelectVal(){
    $.getJSON("server.php",{bigname:$("#bigname").val()},function(json){
        var smallname = $("#smallname");
        $("option",smallname).remove(); //清空原有的选项
        $.each(json,function(index,array){
            var option = "<option value='"+array['id']+"'>"+array['title']+"</option>";
            smallname.append(option);
        });
    });
}
$(function(){
    getSelectVal();
    $("#bigname").change(function(){
        getSelectVal();
    });
});
//上传图片

function setImagePreview(avalue) {
            var docObj=document.getElementById("doc");

            var imgObjPreview=document.getElementById("preview");
            if(docObj.files &&docObj.files[0])
            {
            //火狐下，直接设img属性
            imgObjPreview.style.display = 'block';
            imgObjPreview.style.width = '120px';
            imgObjPreview.style.height = '150px';
            //imgObjPreview.src = docObj.files[0].getAsDataURL();

            //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式
            imgObjPreview.src = window.URL.createObjectURL(docObj.files[0]);
            }
            else
            {
            //IE下，使用滤镜
            docObj.select();
            var imgSrc = document.selection.createRange().text;
            var localImagId = document.getElementById("localImag");
            //必须设置初始大小
            localImagId.style.width = "120px";
            localImagId.style.height = "150px";
            //图片异常的捕捉，防止用户修改后缀来伪造图片
            try{
            localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
            localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc;
            }
            catch(e)
            {
            alert("您上传的图片格式不正确，请重新选择!");
            return false;
            }
            imgObjPreview.style.display = 'none';
            document.selection.empty();
            }
            return true;
            }
//性别选择按钮

$(function () {
                $("[data-toggle='popover']").popover();
            });
            function radio1(){
                document.getElementById('sxp1').style.border = '2px dotted gray';
                document.getElementById('sxp2').style.border = '0';
            }
            function radio2(){
                document.getElementById('sxp2').style.border = '2px dotted gray';
                document.getElementById('sxp1').style.border = '0';
            }
//textarea点击消失提示

            var t = document.getElementById('t');
            t.onfocus = function(){
                if(this.innerHTML == '（请从学习成绩、任职经历、获奖情况、个人特长、爱好、申请职位相关技能等方面进行自我说明，并进行自我评价）'){this.value = ''}
            };
            t.onblur = function(){
                if(this.value == ''){
                    this.innerHTML = '（请从学习成绩、任职经历、获奖情况、个人特长、爱好、申请职位相关技能等方面进行自我说明，并进行自我评价）'}
            };
//验证必要信息是否完整

function checki(){
                if(document.myform.name.value==""){
                    $("#myAlert2").css('display','block');//姓名
                    return false;
                }
                else if(document.myform.sex.value==""){
                    $("#myAlert2").css('display','block');//性别
                    return false;
                }
                else if(document.myform.political_status.value==""){
                    $("#myAlert2").css('display','block');//政治面貌
                    return false;
                }
                else if(document.myform.date_of_birth.value==""){
                    $("#myAlert2").css('display','block');//出生日期
                    return false;
                }
                else if(document.myform.photo.value==""){
                    $("#myAlert2").css('display','block');//上传照片
                    return false;
                }
                else if(document.myform.professional_class.value==""){
                    $("#myAlert2").css('display','block');//专业班级
                    return false;
                }
                else if(document.myform.dormitory.value==""){
                    $("#myAlert2").css('display','block');//宿舍
                    return false;
                }
                else if(document.myform.native_place.value==""){
                    $("#myAlert2").css('display','block');//籍贯
                    return false;
                }
                else if(document.myform.telephone.value==""){
                    $("#myAlert2").css('display','block');//电话
                    return false;
                }
                else if(document.myform.qq.value==""){
                    $("#myAlert2").css('display','block');//QQ
                    return false;
                }
                else if(document.myform.specialty.value==""){
                    $("#myAlert2").css('display','block');//特长
                    return false;
                }
                else{
                    $("#myAlert1").css('display','block');
                    $("#myAlert2").css('display','none');//提交成功
                    return true;
                }

            };
//将数据传入iframe中 避免提交按钮刷新页面
            // document.forms[0].target="rfFrame";
            // $("#f2").attr("target","rfFrame");
//实现div切换

function setTab(name, cursel, n) {
for (i = 1; i <= n; i++) {
var menu = document.getElementById(name + i);
var con = document.getElementById("con_" + name + "_" + i);
menu.className = i == cursel ? "curr": "";
con.style.display = i == cursel ? "block": "none";

}
}